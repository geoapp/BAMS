package com.pinhuba.bams.view;

import com.pinhuba.bams.framework.util.SharePrefsHelper;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;

public class PreferenceSettingActivity extends PreferenceActivity {
	private static SharedPreferences sp = null;
	private static Editor editor = null;
	private SharePrefsHelper spHelper;
	private PreferenceScreen ps;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(com.pinhuba.bams.R.xml.setting);
		ps = (PreferenceScreen) findPreference("ps_doanlowd_path");
		spHelper = new SharePrefsHelper();
	}

	@Override
	protected void onResume() {
		super.onResume();
		ps.setSummary(spHelper.getSavePath());
	}

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
			Preference preference) {
		sp = preference.getSharedPreferences();
		if (sp != null) {
			editor = sp.edit();
		}
		return false;
	}
}
