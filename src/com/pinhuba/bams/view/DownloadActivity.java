package com.pinhuba.bams.view;

import java.io.File;
import com.pinhuba.bams.R;
import com.pinhuba.bams.framework.ErrorCode;
import com.pinhuba.bams.framework.UrlType;
import com.pinhuba.bams.framework.controller.AsynUpgrade;
import com.pinhuba.bams.framework.controller.Controller;
import com.pinhuba.bams.framework.controller.ErrorListener;
import com.pinhuba.bams.framework.util.SharePrefsHelper;
import com.pinhuba.bams.framework.util.ToastHelper;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class DownloadActivity extends Activity {
	private TextView downloadingVersion;
	private ProgressBar progressBar;
	private TextView progressTv;
	private Button hideBtn;
	private Button cancelBtn;

	private String versionName;
	private String versionURL;

	private AsynUpgrade upgradeAsync;
	private SharePrefsHelper downloadHelper;
	private String filePath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.upgrade_dialog);
		initView();
		initData();
		setListener();
	}

	private void initView() {
		downloadingVersion = (TextView) findViewById(R.id.upgrade_downloading_version);
		progressBar = (ProgressBar) findViewById(R.id.upgrade_pb);
		progressTv = (TextView) findViewById(R.id.upgrade_progress_tv);
		hideBtn = (Button) findViewById(R.id.upgrade_hideWindow_bt);
		cancelBtn = (Button) findViewById(R.id.upgrade_canceldownload_bt);
	}

	private void initData() {
		versionName = getIntent().getStringExtra("versionName");
		versionURL = getIntent().getStringExtra("versionURL");
		progressBar.setMax(100);
		downloadHelper = new SharePrefsHelper();
		downloadingVersion.setText(getResources().getString(R.string.now_downlaoding) + versionName);
		filePath = downloadHelper.getSavePath() + "/" + versionName;
		upgradeAsync = Controller.getInstance().doUpgrade(getApplicationContext(), errorListener, filePath, versionURL, progressTv, progressBar, null);
	}

	private void setListener() {
		hideBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DownloadActivity.this.finish();
			}
		});
		cancelBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				upgradeAsync.cancel(true);
				DownloadActivity.this.finish();
			}
		});
	}

	private ErrorListener errorListener = new ErrorListener() {
		public void onSuccess(UrlType type, String json) {
			try {
				File file = new File(filePath);
				Intent intent = new Intent();
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.setAction(android.content.Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
				getApplicationContext().startActivity(intent);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				DownloadActivity.this.finish();
			}
		}

		public void onError(UrlType type, ErrorCode code) {
			switch (code) {
			case READW_TIMOUT:
				ToastHelper.showShortMsg(R.string.exception_readw_timeout);
				break;
			case AIRPLANE_MODE:
				ToastHelper.showShortMsg(R.string.exception_airplane_mode);
				break;
			case NO_INTERNET:
				ToastHelper.showShortMsg(R.string.exception_no_internet);
				break;
			case INVALID_IO:
				ToastHelper.showShortMsg(R.string.exception_invaild_io);
				break;
			case NOSDCARD:
				ToastHelper.showShortMsg(R.string.exception_nosdcard);
				break;
			case INVALID_REQUEST:
				ToastHelper.showShortMsg(R.string.exception_invaild_request);
				break;
			case UNKNOWN_EXCEPTION:
				ToastHelper.showShortMsg(R.string.unknow_wrong);
				break;
			default:
				break;
			}
		}
	};
}
