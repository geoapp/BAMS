package com.pinhuba.bams.framework;

public enum ErrorCode {
	SUCCESS,

	// request
	UNKNOW_REQUEST, INVALID_REQUEST, INVALID_IO,

	// bad arguments
	BAD_ARGS,

	// network
	NO_INTERNET, AIRPLANE_MODE, WIFIOR3G_OFFLINE, CONNECT_TIMEOUT, READW_TIMOUT,

	// sdcard
	NOSDCARD, SDCARD_FULL,

	// file
	FILE_EXIST,

	INVALID_ACCOUNT, INVALID_PASSWORD,

	INVALID_MD5,

	// unknown
	UNKNOWN_EXCEPTION
}
