package com.pinhuba.bams.framework.controller;

import com.pinhuba.bams.framework.ErrorCode;
import com.pinhuba.bams.framework.UrlType;

public interface ErrorListener {
	
	void onSuccess(UrlType type, String json);

	void onError(UrlType type, ErrorCode code);
}
