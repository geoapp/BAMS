package com.pinhuba.bams.framework.controller;

import java.io.IOException;
import java.util.HashMap;
import com.pinhuba.bams.framework.ConfigProperties;
import com.pinhuba.bams.framework.Url;
import com.pinhuba.bams.framework.UrlType;
import com.pinhuba.bams.framework.exception.NetworkException;
import com.pinhuba.bams.framework.exception.TimeoutException;
import com.pinhuba.bams.framework.http.HttpGetUtil;
import com.pinhuba.bams.framework.http.HttpPostUtil;

public class Handler {

	/**
	 * 执行post请求
	 * 
	 * @param type
	 * @param params
	 * @return
	 * @throws TimeoutException
	 * @throws NetworkException
	 * @throws IOException
	 */
	public static String doPost(UrlType type, HashMap<String, String> params) throws TimeoutException, NetworkException, IOException {
		String url = ConfigProperties.SERVER_URL + Url.getUrl(type);
		return HttpPostUtil.executeRequest(url, params);
	}

	/**
	 * 执行get请求
	 * 
	 * @param type
	 * @param params
	 * @return
	 * @throws TimeoutException
	 * @throws NetworkException
	 * @throws IOException
	 */
	public static String doGet(UrlType type, HashMap<String, String> params) throws TimeoutException, NetworkException, IOException {
		String url = ConfigProperties.SERVER_URL + Url.getUrl(type);
		return HttpGetUtil.executeRequest(url, params);
	}

}
