package com.pinhuba.bams.framework;

import java.util.HashMap;
import com.pinhuba.bams.framework.ConfigProperties;

public class Params {

	private static String sessionId = null;
	private static int defaultPageSize = ConfigProperties.DefaultPageSize;

	public static void setUserSessionId(String userSessionId) {
		sessionId = userSessionId;
	}

	public static String getUserSessionId() {
		return sessionId;
	}

	/* 登录 */
	public static HashMap<String, String> login(String username, String password) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("companyCode", ConfigProperties.COMPANY_CODE);
		map.put("username", username);
		map.put("password", password);
		return map;
	}

	/* 公告列表 */
	public static HashMap<String, String> announceList(int currentPage, int pageSize) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("sessionId", sessionId);
		map.put("currentPage", Integer.toString(currentPage));
		map.put("pageSize", Integer.toString(pageSize <= 0 ? defaultPageSize : pageSize));
		return map;
	}

	/* 公告内容 */
	public static HashMap<String, String> announceDetail(String id) {
		HashMap<String, String> mMap = new HashMap<String, String>();
		mMap.put("sessionId", sessionId);
		mMap.put("id", id);
		return mMap;
	}
}
