package com.pinhuba.bams.framework;

public class Constants {
	public final static String USER_LOGIN_ACCOUNT = "UserLoginAccount";
	public final static String USER_LOGIN_PWD = "UserLoginPwd";
	public final static String DOWNLOAD_PATH = "DownLoadPath";
	public final static String SAVE_PWD = "SavePwd";
	public final static String AUTO_LOGIN = "AutoLogin";
	public final static String SESSIONID = "SessionId";
	public final static String USERNAME = "UserName";
	public final static String SAVEPATH = "SavePath";
	public final static String VOICEONOFF = "VoiceOnOff";
	public final static String TIMEREFRESHPERIOD = "TimeRefreshPeriod";
}
