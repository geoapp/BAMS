package com.pinhuba.bams.framework.exception;

public class NetworkException extends IException{
	
	private static final long serialVersionUID = 1L;

	protected String errCode = null; 
	
	public NetworkException() {
        super();
    }
	
	public NetworkException(Exception exception) {
        super(exception);
    }
	
    public NetworkException(String errorCode) {
        this.errCode = errorCode; 
    }

    public NetworkException(String errorCode, String errMsg) {
    	super(errMsg);
        this.errCode = errorCode; 
    }
    
    public NetworkException(String errorCode, Exception exception) {
        super(errorCode, exception);
    }
    
    /**
     * ???�??�?��?��???rrorCode�?????常�?�?ode?��?�??-1
     * @Title: getErrorCode
     * @return error code??
     */
    public String getErrorCode()
    {
    	return errCode;
    }
}
