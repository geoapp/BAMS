package com.pinhuba.bams.framework.exception;

public class TimeoutException extends NetworkException
{
	private static final long serialVersionUID = 1L;

	public TimeoutException() {
        super();
    }

	public TimeoutException(String detailMessage) {
        super(detailMessage);
    }

    public TimeoutException(String detailMessage, Exception exception) {
        super(detailMessage, exception);
    }
}
