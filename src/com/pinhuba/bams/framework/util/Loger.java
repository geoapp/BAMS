package com.pinhuba.bams.framework.util;

import com.pinhuba.bams.framework.ConfigProperties;

import android.util.Log;

public class Loger {
	private static boolean CLOSE = ConfigProperties.LOGER_CLOSE;

	public static void v(String msg) {
		if (CLOSE)
			return;

		final StackTraceElement[] stack = new Throwable().getStackTrace();
		final int i = 1;
		final StackTraceElement ste = stack[i];

		Log.v(ste.getClassName(), String.format("[%s][%d]%s", ste.getMethodName(), ste.getLineNumber(), msg));
	}

	public static void d(String msg) {
		if (CLOSE)
			return;

		final StackTraceElement[] stack = new Throwable().getStackTrace();
		final int i = 1;
		final StackTraceElement ste = stack[i];

		Log.d(ste.getClassName(), String.format("[%s][%d]%s", ste.getMethodName(), ste.getLineNumber(), msg));
	}

	public static void i(String msg) {
		if (CLOSE)
			return;

		final StackTraceElement[] stack = new Throwable().getStackTrace();
		final int i = 1;
		final StackTraceElement ste = stack[i];

		Log.i(ste.getClassName(), String.format("[%s][%d]%s", ste.getMethodName(), ste.getLineNumber(), msg));
	}

	public static void w(String msg) {
		if (CLOSE)
			return;

		final StackTraceElement[] stack = new Throwable().getStackTrace();
		final int i = 1;
		final StackTraceElement ste = stack[i];

		Log.w(ste.getClassName(), String.format("[%s][%d]%s", ste.getMethodName(), ste.getLineNumber(), msg));
	}

	public static void e(String msg) {
		if (CLOSE)
			return;

		final StackTraceElement[] stack = new Throwable().getStackTrace();
		final int i = 1;
		final StackTraceElement ste = stack[i];

		Log.e(ste.getClassName(), String.format("[%s][%d]%s", ste.getMethodName(), ste.getLineNumber(), msg));
	}

}
