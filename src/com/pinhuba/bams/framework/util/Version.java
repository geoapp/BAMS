package com.pinhuba.bams.framework.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.telephony.TelephonyManager;

/**
 * Application version related tools
 */
public class Version {

	/**
	 * Gets the version number
	 * 
	 * @return
	 */
	public static String getVersionName() {
		Context context = MyApplication.getAppContext();
		PackageManager packageManager = context.getPackageManager();
		// GetPackageName () is your current class package name, 0 stands for is
		// to get version information
		PackageInfo packInfo;
		try {
			packInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			Loger.d(packInfo.versionName);
			return packInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * The App for current versionCode (from 1 started), if not find
	 * (NameNotFoundException), return zero
	 * 
	 * @Title: getVersionCode
	 * @return
	 */
	public static int getVersionCode() {
		Context context = MyApplication.getAppContext();
		PackageManager packageManager = context.getPackageManager();
		// GetPackageName () is your current class package name, 0 stands for is
		// to get version information
		PackageInfo packInfo;
		try {
			packInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			Loger.d("versionCode = " + packInfo.versionCode);
			return packInfo.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 返回SDK版本
	 * 
	 * @return
	 */
	public static int getVersionSDK() {
		return Build.VERSION.SDK_INT;
	}

	/**
	 * Get device id (IMEI number)
	 * 
	 * @return
	 */
	public static String getDeviceID() {
		Context context = MyApplication.getAppContext();
		return ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
	}
}
