package com.pinhuba.bams.framework;

import java.util.HashMap;

public class Url {

	private static HashMap<UrlType, String> urlMap;

	static {
		urlMap = new HashMap<UrlType, String>();
		urlMap.put(UrlType.LOGINCHECK, "loginCheck.do");
		urlMap.put(UrlType.ANNOUNCE_LIST, "announceList.do");
		urlMap.put(UrlType.ANNOUNCE_DETAIL, "announceDetail.do");
	}

	public static String getUrl(UrlType urlType) {
		return urlMap.get(urlType);
	}
}
